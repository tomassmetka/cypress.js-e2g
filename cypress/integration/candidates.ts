/// <reference path="../support/index.d.ts" />
context('Candidates', () => {
    beforeEach(() => {
        cy.login()
        cy.visit('/candidate')
        cy.wait(1500)
    })


    it('Test download files ', () => {
        cy.wait(3000)
        cy.xpath('//*[@id="content-inner"]/div[1]/form/div[2]/div[1]/a[3]').click({force: true})
    })

    it('Filtration test in grid ', () => {
        let i;
        // Test first 2 filters, ignore first
        for (i = 2; i <= 2; i++) {
            cy.xpath(`//*[@id="content-inner"]/div[1]/form/div[1]/div[2]/table/thead/tr/th[${i + 1}]/a`).click({force: true});
        }
    })

    it('Paginator ', () => {
        cy.xpath('//*[@id="content-inner"]/div[1]/form/div[2]/div[2]/ul/li[3]/a[1]').click({force: true})
        cy.xpath('//*[@id="content-inner"]/div[1]/form/div[2]/div[2]/ul/li[1]/a[2]').click({force: true})
    })
})

