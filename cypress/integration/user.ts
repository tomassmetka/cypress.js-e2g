/// <reference path="../support/index.d.ts" />
context('User', () => {
    before(() => {
        cy.server()
        cy.login()
        cy.visit('/user/filter')
        cy.wait(1500)
    })

    it('Test filter', () => {
        cy.wait(2000)
        let i;
        // Test first 2 filters, ignore first
        for (i = 2; i <= 3; i++) {
            cy.xpath(`/html/body/div[1]/div/div[1]/div[2]/div[2]/div/div/div[1]/div[2]/table/thead/tr/th[${1 + i}]/a`).click()
        }
    })

    it('Test pagination', () => {
        cy.xpath('/html/body/div[1]/div/div[1]/div[2]/div[2]/div/div/div[2]/div[2]/ul/li[3]/a[1]').click()
        cy.xpath('/html/body/div[1]/div/div[1]/div[2]/div[2]/div/div/div[2]/div[2]/ul/li[1]/a[2]').click()
        cy.xpath('/html/body/div[1]/div/div[1]/div[2]/div[2]/div/div/div[2]/div[2]/ul/li[3]/a[2]').click()
        cy.xpath('/html/body/div[1]/div/div[1]/div[2]/div[2]/div/div/div[2]/div[2]/ul/li[1]/a[1]').click()
    })

    it('Test filter', () => {
        cy.xpath('/html/body/div[1]/div/div[1]/div[2]/div[2]/div/form[1]/div/div[2]/a').click()
        cy.xpath('/html/body/div[1]/div/div[1]/div[2]/div[2]/div/form[1]/div/div[1]/div/div[1]/div[1]/div/select[1]')
        cy.xpath('/html/body/div[1]/div/div[1]/div[2]/div[2]/div/form[1]/div/div[1]/div/div[2]/a').click()
    })
})

