/// <reference path="../support/index.d.ts" />
context('Menu', () => {
    it('Navigation in left side panel', () => {
        cy.login()
        cy.get('.menu-list > li').each(($el, $index) => {
           cy.xpath(`//*[@id="menu"]/div[1]/ul/li[${$index + 1}]/a`).click()
        })
    })
})
