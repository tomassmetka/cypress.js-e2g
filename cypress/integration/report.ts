/// <reference path="../support/index.d.ts" />
context('Report', () => {
    beforeEach(() => {
        cy.login()
        cy.server()
        cy.visit('/report')
        cy.wait(1500)
    })


    /*
    it('Select report ', () => {
        cy.xpath('//*[@id="content-inner"]/div/div/div[2]/form/select')
            .find('option')
            .then($elm => $elm.get(1).setAttribute('selected', "selected"))
            .parent()
            .trigger('change')

        let i;
        // Test first 2 filters, ignore first
        for (i = 2; i <= 2; i++) {
            cy.xpath(`//*[@id="content-inner"]/div[2]/div[1]/div[2]/table/thead/tr/th[${i + 1}]/a`).click({force: true});
        }
    })
     */

    it('Test report tab ', () => {
        cy.xpath('//*[@id="content-inner"]/div/div/div[2]/form/select')
            .find('option')
            .then($elm => $elm.get(1).setAttribute('selected', "selected"))
            .parent()
            .trigger('change')

        cy.wait(15000)

        cy.xpath('//*[@id="filter-rjsfilter"]/div/div[1]/div[2]/div[2]/div[1]/button').click()
        let x;
        // Test first 2 filters, ignore first
        for (x = 2; x <= 5; x++) {
            cy.xpath(`//*[@id="filter-rjsfilter"]/div/div[1]/div[2]/div[2]/div[1]/ul/li[${x + 1}]`).click()
        }

        // click outside
        cy.xpath('//*[@id="filter-rjsfilter"]/div/div[1]/div[2]').click({ force: true})
        cy.xpath(`//*[@id="filter-rjsfilter"]/div/div[1]/div[3]/button`).click()


        cy.xpath('/html/body/div[1]/div/div[1]/div[2]/div[2]/div/div[2]/div[1]/div[2]/table/tbody/tr[1]').then(elem => {
            if (elem) {
                cy.visit(elem[0].getAttribute('data-grid-route'))
            } else {
                return 0
            }
        });

        let i;
        // Test first 2 filters, ignore first
        for (i = 2; i <= 3; i++) {
            cy.xpath(`//*[@id="nav-tab"]/li[${i + 1}]/a`).click()
            cy.xpath(`//*[@id="nav-tab"]/li[${i + 1}]/a`).should('have.class', 'active')
        }
    })
})

