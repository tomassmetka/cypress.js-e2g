/// <reference path="../support/index.d.ts" />
context('Position', () => {
    it('Test save position form', () => {
        // todo docasne vypnute pro klikaci typ
        return;
        cy.login()
        cy.get('.inner-box .plus').click()
        cy.wait(500)
        cy.get('.add-dropdown').should('be.visible')
        cy.get('.add-dropdown a').first().click()

        // Save form
        cy.get('#name').type('Hello world')
        cy.get('#division').select('commercial', {force: true})
        cy.get('#department').select('Acquisition Cash Loan', {force: true})
        cy.get('#section').select('Acquisition Cash Loan', {force: true})

        cy.get('#superior_firstname').type('First name')
        cy.get('#superior_lastname').type('Last name')
        cy.get('#superior_email').type('foo@bar.cz')
        cy.get('#superior_position_name').type('First name')

        cy.get('#org_level').select('CEO-1', {force: true})
        cy.get('#replacement').select('New position', {force: true})
        cy.get('#budget').select('over', {force: true})
        cy.get('#expertise').select('Assistant', {force: true})
        cy.get('#province').select('TP. Hồ Chí Minh', {force: true})

        cy.setTinyMceContent('description', 'Example description')
        cy.setTinyMceContent('requirements', 'Example requirements')

        cy.get('#searching_tags').type('Hello world')

        cy.get('#position_create').submit()
        cy.get('.alert-danger').should('be.visible')
    })
})
